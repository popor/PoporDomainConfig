//
//  PoporDomainConfigVC.h
//  PoporDomainConfig
//
//  Created by apple on 2019/6/12.
//  Copyright © 2019 popor. All rights reserved.

#import <UIKit/UIKit.h>

@class PoporDomainConfig;

NS_ASSUME_NONNULL_BEGIN

@interface PoporDomainConfigVC : UIViewController 

// UI
@property (nonatomic, strong) UICollectionView * headCV;

@property (nonatomic, strong) UITextField      * defaultUrlTF;
@property (nonatomic, strong) UIButton         * saveBT;
@property (nonatomic, strong) UILabel          * infoL;
@property (nonatomic, strong) UITableView      * infoTV;

// other
@property (nonatomic, weak  ) PoporDomainConfig      * domainConfig;
@property (nonatomic, strong) UIFont                 * cellFont;
@property (nonatomic        ) CGFloat                  screenW;
@property (nonatomic        ) NSInteger                cvSelectIndex;
@property (nonatomic, strong) UITapGestureRecognizer * tapEndEditGR;

@end

NS_ASSUME_NONNULL_END
