//
//  PoporDomainConfigEntity.m
//  PoporDomainConfig_Example
//
//  Created by apple on 2019/6/12.
//  Copyright © 2019 popor. All rights reserved.
//

#import "PoporDomainConfigEntity.h"

@implementation PoporDomainConfigUE

- (id)initTitle:(NSString *)title withDomain:(NSString *)domain {
    if (self = [super init]) {
        _title  = title;
        _domain = domain;
    }
    return self;
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

@implementation PoporDomainConfigLE

- (id)init {
    if (self = [super init]) {
        _ueArray = [NSMutableArray<PoporDomainConfigUE> new];
    }
    return self;
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

@implementation PoporDomainConfigEntity

- (id)init {
    if (self = [super init]) {
        _leArray = [NSMutableArray<PoporDomainConfigLE> new];
    }
    return self;
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end
